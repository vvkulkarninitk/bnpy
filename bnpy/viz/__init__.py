"""
The :mod:`viz` module provides visualization capability
"""

import GaussViz
import PlotELBO
import PlotComps

__all__ = ['GaussViz', 'PlotELBO', 'PlotComps']
